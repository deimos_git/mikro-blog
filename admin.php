<?php
session_start();
$_SESSION['head-title'] = 'Blog Admin Panel';


if (isset($_GET['exit']) and $_GET['exit'] == 'exit') {
    session_unset();
}
 if(!isset($_SESSION['logged']) || $_SESSION['role'] != 'admin'){
 	header('Location: profile.php');
 }

// $_SESSION['adminPanel'] = true;
$_SESSION['init'] = true; 


if (!empty($_POST)){

	/*
	Add new user
    */
    if (isset($_POST['add-login']) and isset($_POST['password']) and isset($_POST['re-password'])){
        $newUser = trim(strip_tags($_POST['add-login']));
        $password = trim(strip_tags($_POST['password']));
        $rePassword = trim(strip_tags($_POST['re-password']));
        $role = $_POST['role'];


        if ($password == $rePassword){
            require_once "controller/userController.php";
            $putUser = new userController();

            $checkedUser = $putUser->checkUser($newUser);
            $resultUserChack = $checkedUser['username'];

            if($resultUserChack == $newUser){
            	$_SESSION['checkedUser'] = "<p class = 'error-message'>This user exist!</p>";
            } else{
            	$putUser->getNewUser($newUser, $password, $role);
            }
        }
        else{
            $_SESSION['ErrorPass'] = "<p class = 'error-message'>Wrong password</p>";
        }
    }

    /*
	Update
    */
    if(isset($_POST['update'])){
    	if(isset($_POST['update-login']) and isset($_POST['new-role'])){
    		$updateLogin = $_POST['update-login'];
    		$newRole = $_POST['new-role'];

    		if($updateLogin != 'admin'){
	    		require_once 'controller/userController.php';
	    		$updateUser = new userController();
	    		$UpdateUser = $updateUser->update($updateLogin, $newRole);
    		} else{
    			$_SESSION['updateAdmin'] = "<p class = 'error-message'>This user can not be edited</p>";
    		}
    	} 
    }

    /*
	Delete
    */
    if(isset($_POST['delete'])){
    	if(isset($_POST['del-login']) and $_POST['del-login'] != 'admin'){
    		$deleteLogin = $_POST['del-login'];
	    		require_once 'controller/userController.php';
	    		$deletUser = new userController();
	    		$unsetUser = $deletUser->delete($deleteLogin);
    	} else{
    		$_SESSION['deleteAdmin'] = "<p class = 'error-message'>This user can not be delete</p>";
    	}
    }

    /* 
	Add Post
    */
	if(isset($_POST['addPost'])){
		if(!empty($_POST['Title']) and !empty($_POST['Excerpt']) and !empty($_POST['Posts'])){
			$authorPost = $_SESSION['username'];
			$titlePost = trim(strip_tags($_POST['Title']));
			$excerptPost = trim($_POST['Excerpt']);
			$newPost = trim($_POST['Posts']);
			if (!empty($_FILES)){
	            $imgPath = "dist/uploads/";
	            $imgFullPath =  $imgPath . $_FILES['img']['name'];
	            move_uploaded_file($_FILES['img']['tmp_name'], $imgPath . $_FILES['img']['name']);
	        } else{
	            $imgFullPath = '';
	        }
			require_once 'controller/postsController.php';
			$post = new postController();
			$addPost = $post->CreateNewPost($authorPost,$titlePost,$excerptPost,$newPost,$imgFullPath);
			unset($imgFullPath);
        	unset($_FILES['img']);
		} else{
			$emptyInputPost = "<p class = 'error-message'>fields must be filled</p>";
			$_SESSION['emptyInputPost'] = $emptyInputPost;
		}
	}

}


require_once "view/header.php";


?>
<div class="container">
	<div class="row">
		<?php 
			if(!empty($_SESSION)){
				if (isset($_SESSION['logged']) && $_SESSION['role'] == 'admin') {
						require_once 'controller/userController.php';
						$user = new userController();
						$allUsers = $user->getUsers();	
						?>
						
							<!-- Modal -->
						<div class="modal fade" id="allUsers" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
						  <div class="modal-dialog" role="document">
						    <div class="modal-content">
						      <div class="modal-header">
						        <h5 class="modal-title" id="exampleModalLabel">Users</h5>
						        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
						          <span aria-hidden="true">&times;</span>
						        </button>
						      </div>
						      <div class="modal-body">
						      	<?php
									foreach ($allUsers as $allUser):
										?>
							        <p>User Name: <strong><?= $allUser['username']; ?></strong> -- Role: <strong><?= $allUser['role']; ?></strong></p>
							        <?php	
									endforeach;
								?>
						      </div>
						      <div class="modal-footer">
						        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
						      </div>
						    </div>
						  </div>
						</div>
						
			<div class="col-md-12">
				<div class="row">
					<div class="col-md-12">
						<h3>User</h3>
					</div>
				</div>
				<div class="row">
					<div class="col-md">
						<!-- Modal add new user -->
						<button type="button" class="btn btn-primary" data-toggle="modal" data-target="#addUserModal">
						  Add new user
						</button>
					</div>
				</div>

				<!-- Modal -->
				<div class="modal fade" id="addUserModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
				  <div class="modal-dialog" role="document">
				    <div class="modal-content">
				      <div class="modal-header">
				        <h5 class="modal-title" id="exampleModalLabel">Add user</h5>
				        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
				          <span aria-hidden="true">&times;</span>
				        </button>
				      </div>
				        <form action="admin.php" method="POST">
				      <div class="modal-body">
							<div class="form-group">
								<label for="add-login">Login</label>
								<input type="text" name="add-login" class="form-control" placeholder="Login">
							</div>
							<div class="form-group">
								<label for="password">Password</label>
								<input type="password" name="password" class="form-control" placeholder="Password">
								<label for="password">retypa Password</label>
								<input type="password" name="re-password" class="form-control" placeholder="retypa Password">
							</div>
							<div class="form-group">
								<select name="role" class="form-control">
									<option value="moderator">moderator</option>
									<option value="admin">Admin</option>
									<option value="user">user</option>
								</select>
							</div>
				      </div>
				      <div class="modal-footer">
				        	<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
							<button type="submit" name="send" class="btn btn-primary">Create</button>
				      </div>
						</form>
				    </div>
				  </div>
				</div>
				<?php 
					if(!empty($_SESSION)){
						if(isset($_SESSION['userMessage'])){
							echo $_SESSION['userMessage'];
							unset($_SESSION['userMessage']);
						}
                        if(isset($_SESSION['userErrorMessage'])){
                            echo $_SESSION['userErrorMessage'];
                            unset($_SESSION['userErrorMessage']);
                        }
                        if(isset($_SESSION['checkedUser'])){
                        	echo $_SESSION['checkedUser'];
                        	unset($_SESSION['checkedUser']);
                        }
                        if(isset($_SESSION['ErrorPass'])){
                        	echo $_SESSION['ErrorPass'];
                        	unset($_SESSION['ErrorPass']);
                        }
                        if (isset($_SESSION['loginError'])){
                            echo $_SESSION['loginError'];
                            unset($_SESSION['loginError']);
                        }
					}
				 ?>
			<hr>

			<div class="row">
				<div class="col-md">
					<!-- Modal Update -->
					<button type="button" class="btn btn-primary" data-toggle="modal" data-target="#updateModal">
					  Update
					</button>
						<!-- Modal All Users -->
						<button type="button" class="btn btn-primary" data-toggle="modal" data-target="#allUsers">
						 All users
						</button>
				</div>
			</div>

			<!-- Modal -->
			<div class="modal fade" id="updateModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
			  <div class="modal-dialog" role="document">
			    <div class="modal-content">
			      <div class="modal-header">
			        <h5 class="modal-title" id="exampleModalLabel">Update user</h5>
			        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
			          <span aria-hidden="true">&times;</span>
			        </button>
			      </div>
				    <form action="admin.php" method="POST">
			      <div class="modal-body">
						<div class="form-group">
							<label for="update-login">Update user</label>
							<input type="text" name="update-login" class="form-control" placeholder="Login">
							<hr>
						</div>
						<div class="form-group">
							<select name="new-role" class="form-control">
								<option value="user">user</option>
								<option value="moderator">moderator</option>
								<option value="admin">Admin</option>
							</select>
						</div>
			      </div>
			      <div class="modal-footer">
		        		<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
						<button type="submit" name="update" class="btn btn-primary">Update</button>
			      </div>
				</form>
			    </div>
			  </div>
			</div>
			<?php 
				if(!empty($_SESSION)){
                    if(isset($_SESSION['updateUserComplate'])){
                    	echo $_SESSION['updateUserComplate'];
                    	unset($_SESSION['updateUserComplate']);
                    } 
                    if(isset($_SESSION['updateUserFail'])){
                    	echo $_SESSION['updateUserFail'];
                    	unset($_SESSION['updateUserFail']);
                    }
                    if(isset($_SESSION['updateAdmin'])){
                    	echo $_SESSION['updateAdmin'];
                    	unset($_SESSION['updateAdmin']);
                    }
				}
			 ?>
			
		<hr>
		<div class="row">
			<div class="col-md">
				<!-- Modal delete -->
				<button type="button" class="btn btn-primary" data-toggle="modal" data-target="#deleteModal">
				  Delete user
				</button>
			</div>
		</div>

		<!-- Modal -->
		<div class="modal fade" id="deleteModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
		  <div class="modal-dialog" role="document">
		    <div class="modal-content">
		      <div class="modal-header">
		        <h5 class="modal-title" id="exampleModalLabel">Delete</h5>
		        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
		          <span aria-hidden="true">&times;</span>
		        </button>
		      </div>
		        <form action="admin.php" method="POST">
		      <div class="modal-body">
						<div class="form-group">
							<label for="del-login">Delete user</label>
							<input type="text" name="del-login" class="form-control" placeholder="Login">
						</div>
		      </div>
		      <div class="modal-footer">
		        	<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
					<button type="submit" name="delete" class="btn btn-primary">Delete</button>
		      </div>
			</form>
		    </div>
		  </div>
		</div>
			<?php 
				if(!empty($_SESSION)){
                    if(isset($_SESSION['delMessage'])){
                    	echo $_SESSION['delMessage'];
                    	unset($_SESSION['delMessage']);
                    }
                    if(isset($_SESSION['delErrorMessega'])){
                    	echo $_SESSION['delErrorMessega'];
                    	unset($_SESSION['delErrorMessega']);
                    }
                    if(isset($_SESSION['deleteAdmin'])){
                        	echo $_SESSION['deleteAdmin'];
                        	unset($_SESSION['deleteAdmin']);
                        }
				}
			 ?>
			<hr>
			<h3>Post</h3>
            <div class="row">
            <div class="col-md">
							<!-- Iframe -->
    <!-- <iframe id="editor" style="width: 800px; height: 300px;"></iframe> -->
		
		<div class="row">
			<div class="col-md">
				<form action="admin.php" method="POST" enctype="multipart/form-data">
		      <div class="modal-body">
						<div class="form-group">
							<label for="Title">Title</label>
							<input type="text" name="Title" class="form-control" placeholder="Title">
						</div>
						<div class="form-group">
							<label for="exampleFormControlFile1">Example file input</label>
    						<input type="file" name="img" class="form-control-file" id="exampleFormControlFile1">
						</div>
						<div class="form-group custom-text">
						<ul class="custom-list">
							<li><a class="btn btn_textarea" value="click" onclick="document.execCommand('undo', false, null);"><i class="fa fa-undo" aria-hidden="true"></i></a></li>
							<li><a class="btn btn_textarea" value="click" onclick="document.execCommand('removeFormat', false, null);"><i class="fa fa-eraser" aria-hidden="true"></i></a></li>
							<li><a class="btn btn_textarea" value="click" onclick="document.execCommand('foreColor', true, 'red');"><i class="fa fa-th-large" aria-hidden="true"></i></a></li>
							<li><a class="btn btn_textarea" value="click" onclick="document.execCommand('FontName', false, 'Arial');"><i class="fa fa-font" aria-hidden="true"></i></a></li>
							<li><a class="btn btn_textarea" value="click" onclick="document.execCommand('heading', false, 'h1');"><i class="fa fa-header" aria-hidden="true"></i></a></li>
							<li><a class="btn btn_textarea" value="click" onclick="document.execCommand ('bold', false, null);"><i class="fa fa-bold" aria-hidden="true"></i></a></li>
							<li><a class="btn btn_textarea" value="click" onclick="document.execCommand('italic', false, null);"><i class="fa fa-italic" aria-hidden="true"></i></a></li>
							<li><a class="btn btn_textarea" value="click" onclick="document.execCommand('underline', false, null);"><i class="fa fa-underline" aria-hidden="true"></i></a></li>
							<li><a class="btn btn_textarea" value="click" onclick="document.execCommand('strikeThrough', false, null);"><i class="fa fa-strikethrough" aria-hidden="true"></i></a></li>
							<li><a class="btn btn_textarea" value="click" onclick="document.execCommand('formatBlock', false, 'BLOCKQUOTE');"><i class="fa fa-quote-right" aria-hidden="true"></i></a></li>
							<li><a class="btn btn_textarea" value="click" onclick="document.execCommand('CreateLink', true, createLinkANDcreateImg('link'));"><i class="fa fa-link" aria-hidden="true"></i></a></li>
							<li><a class="btn btn_textarea" value="click" onclick="document.execCommand('insertUnorderedList', false, null);"><i class="fa fa-list-ul" aria-hidden="true"></i></a></li>
							<li><a class="btn btn_textarea" value="click" onclick="document.execCommand('insertOrderedList', false, null);"><i class="fa fa-list-ol" aria-hidden="true"></i></a></li>
							<li><a class="btn btn_textarea" value="click" onclick="document.execCommand('insertImage', false, createLinkANDcreateImg('img'));"><i class="fa fa-picture-o" aria-hidden="true"></i></a></li>
						</ul>
							<div class="form-control textFrame1" contenteditable = 'true'></div>
							<textarea name="Excerpt" id="Excerpt"></textarea>
					</div>
						<div class="form-group custom-text">
						<ul class="custom-list">
							<li><a class="btn btn_textarea" value="click" onclick="document.execCommand('undo', false, null);"><i class="fa fa-undo" aria-hidden="true"></i></a></li>
							<li><a class="btn btn_textarea" value="click" onclick="document.execCommand('removeFormat', false, null);"><i class="fa fa-eraser" aria-hidden="true"></i></a></li>
							<li><a class="btn btn_textarea" value="click" onclick="document.execCommand('foreColor', true, 'red');"><i class="fa fa-th-large" aria-hidden="true"></i></a></li>
							<li><a class="btn btn_textarea" value="click" onclick="document.execCommand('FontName', false, 'Arial');"><i class="fa fa-font" aria-hidden="true"></i></a></li>
							<li><a class="btn btn_textarea" value="click" onclick="document.execCommand('heading', false, 'h1');"><i class="fa fa-header" aria-hidden="true"></i></a></li>
							<li><a class="btn btn_textarea" value="click" onclick="document.execCommand ('bold', false, null);"><i class="fa fa-bold" aria-hidden="true"></i></a></li>
							<li><a class="btn btn_textarea" value="click" onclick="document.execCommand('italic', false, null);"><i class="fa fa-italic" aria-hidden="true"></i></a></li>
							<li><a class="btn btn_textarea" value="click" onclick="document.execCommand('underline', false, null);"><i class="fa fa-underline" aria-hidden="true"></i></a></li>
							<li><a class="btn btn_textarea" value="click" onclick="document.execCommand('strikeThrough', false, null);"><i class="fa fa-strikethrough" aria-hidden="true"></i></a></li>
							<li><a class="btn btn_textarea" value="click" onclick="document.execCommand('formatBlock', false, 'BLOCKQUOTE');"><i class="fa fa-quote-right" aria-hidden="true"></i></a></li>
							<li><a class="btn btn_textarea" value="click" onclick="document.execCommand('CreateLink', true, createLinkANDcreateImg('link'));"><i class="fa fa-link" aria-hidden="true"></i></a></li>
							<li><a class="btn btn_textarea" value="click" onclick="document.execCommand('insertUnorderedList', false, null);"><i class="fa fa-list-ul" aria-hidden="true"></i></a></li>
							<li><a class="btn btn_textarea" value="click" onclick="document.execCommand('insertOrderedList', false, null);"><i class="fa fa-list-ol" aria-hidden="true"></i></a></li>
							<li><a class="btn btn_textarea" value="click" onclick="document.execCommand('insertImage', false, createLinkANDcreateImg('img'));"><i class="fa fa-picture-o" aria-hidden="true"></i></a></li>
						</ul>
								<div class="form-control textFrame2" contenteditable = 'true'></div>
									<textarea name="Posts" id = "Posts"></textarea>
		      </div>
		      <div class="modal-footer">
					<button type="submit" name="addPost" onclick="replaceDiv()" class="btn btn-primary">Create</button>
		      </div>
			</form>
			<?php 
					if(!empty($_SESSION)){
		                    if(isset($_SESSION['emptyInputPost'])){
		                    	echo $_SESSION['emptyInputPost'];
		                    	unset($_SESSION['emptyInputPost']);
		                    }
		                    if(isset($_SESSION['AddedPost'])){
		                    	echo $_SESSION['AddedPost'];
		                    	unset($_SESSION['AddedPost']);
		                    }
		                    if(isset($_SESSION['crashPost'])){
		                    	echo $_SESSION['crashPost'];
		                    	unset($_SESSION['crashPost']);
		                    }

		                }
				 ?>
			</div>
		</div>

		</div>
		<?php
				} 
			}
		 ?>
		
	</div>
</div>

 <?php 
	require_once 'view/footer.php';
  ?>