<?php 
	session_start();
	$_SESSION['init'] = true; 
	$_SESSION['head-title'] = 'Blog';
	
	require_once 'view/header.php';
	require_once "controller/postsController.php";
	
if (isset($_POST['deletePost'])) {
    $delPost = $_POST['deletePost'];
    $deletePost = new postController();
    $deletePost->delPost($delPost);
}


//class test{
//
//    private static $counter = 0;
//    public function __construct()
//    {
//        self::$counter +=1;
//    }
//
//    public static function _static(){
//        return self::$counter;
//    }
//
//}
//$obj1 = new test();

 ?>
<div class="container">
    <div class="row">
        <div class="col-md">
            <h3><?php
//                echo $obj1::_static();
                ?>
            </h3>
        </div>
    </div>
</div>
 <div class="container">
	<div class="row">
		<div class="col-md">
			<h1>Posts</h1>
		</div>
	</div>
	<div class="row">
		<div class="col-md">
			 <?php 
				$count = 5;
		        $post = new postController();
	            if(!empty($_GET)){
	                $postAuthor=$_GET['post_author'];
	                $allPosts = $post->sortPostA($postAuthor);
	            }
		        $allPosts = $post->outputPosts($start = 0, $count);
		        $postsCount = count($allPosts);
		        $len = floor( $postsCount / $count);

		        if (isset($_GET['page'])){
		            $page = $_GET['page'];
		            if ($page == 1){
		                $start = 0;
		                $allPosts = $post->outputPosts($start, $count);
		            }
		            else {
		                $start = 5;
		                $allPosts = $post->outputPosts($start, $count);
		            }
		        }

				foreach ($allPosts as $post):
					 if (empty($post['image'])) {
		                $post['image'] = '/dist/uploads/default.jpg';
		            }
					?>
				<div class="row posts">
                    <div class="row">
                    	<div class="col-md">
                    		<h4><a href="/view/post.php?post_id=<?= $post['id'] ?>"><?= $post['title'] ?></a></h4>
	                        <a href="/view/post.php?post_id=<?= $post['id'] ?>" class="thumbnail">
	                            <img src="<?= $post['image'] ?>" alt="default">
	                        </a>
	                    </div>
                    </div>
                    <div class="row">
	                    <div class="col-md post-preview">
	                        <p>
	                            <?= $post['excerpt'] ?>
	                        </p>

	                        <p><a class="btn btn-info" href="/view/post.php?post_id=<?= $post['id'] ?>">Read more</a>
	                        </p>
	                        <br/>
	                        <ul class="list-inline">
	                            <li><i class="glyphicon glyphicon-user"></i> by 
	                            	<a href="index.php?post_author=<?= $post['author']?>"><?= $post['author'] ?></a> |
	                            <i class="glyphicon glyphicon-calendar"></i> <?= $post['date'] ?></li>
	                        </ul>
	                        <?
	                        if (isset($_SESSION['role']) and $_SESSION['role'] == 'admin') {
	                            ?>
	                            <form action="index.php" METHOD="post">
	                                <button name="deletePost" type="submit" value="<?= $post['id'] ?>"
	                                        class="btn btn-danger btn-sm">
	                                    Delete post
	                                </button>
	                            </form>
	                            <?php
	                        }
	                        ?>
	                    </div>
                    </div>
                </div>
                <br>
					<?php
				endforeach;
			 ?> 
			<div class="row">
	            <div class="col-xs-12">
	            	<nav aria-label="Page navigation example">
					  <ul class="pagination">
					    <li class="page-item disabled"><a class="page-link" href="?page=<?= $i + 1 ?>">Previous</a></li>
						    <? for ($i = 0; $i <= $len; $i++): ?>
		                        <li class="page-item"><a class="page-link" href="?page=<?= $i + 1 ?>"><?= $i + 1 ?></a></li>
		                    <? endfor; ?>
					    <li class="page-item disabled"><a class="page-link" href="?page=<?= $i + 1 ?>">Next</a></li>
					  </ul>
					</nav>
	            </div>
	        </div>
		</div>
	</div>
 </div>
 <?php 
	require_once 'view/footer.php';
  ?>
