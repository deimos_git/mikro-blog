<?php

require_once "config.php";

class dbPostManager{

	private $mysqli;

	public function __construct() {
		$this->mysqli = new mysqli(HOST, dbUser, dbUserPass, dbName) or die("Unable to connect");
		$this->mysqli->query("SET NAMES 'utf8'");
	}

  public function CreatePost($authorPost,$titlePost,$excerptPost,$newPost,$imgFullPath){
    date_default_timezone_set('Europe/Kiev');
    $time = date('Y-m-d H:i:s');

    $result =  $this->mysqli->query("INSERT INTO Posts(id, author, title, excerpt, fullPost, date, image) 
      VALUES (NULL, '$authorPost', '$titlePost', '$excerptPost', '$newPost', '$time', '$imgFullPath')");
     return $result;
  }

  public function getFullPost($postID){
    $result = $this->mysqli->query("SELECT `title`, `fullPost`, `author`, `date`, `image` FROM `Posts` WHERE `id` = '$postID'");
    return $result;
  }

  public function Output($start, $count){
     $result = $this->mysqli->query("SELECT `id`, `title`, `author`, `excerpt`, `date`, `image` FROM `Posts` ORDER BY `date` DESC  LIMIT $start, $count");
        return $result;
  }
  public function delPost($delpost){
        $result = $this->mysqli->query("DELETE  FROM `Posts` WHERE `id` = '$delpost'");
        return $result;
    }

    public function sortPostByAuthor($authorP){
    $result = $this->mysqli->query("SELECT `id`, `author`, `title`, `excerpt`, `date`,`image` FROM `Posts` WHERE `author` = '$authorP' ORDER BY `author`,`date` DESC ");
    return $result;
    }

	public function __destruct() {
		if ($this->mysqli) {
			$this->mysqli->close();
		}
	}
}