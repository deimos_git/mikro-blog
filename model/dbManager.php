<?php

require_once "config.php";

class dbManager{

	private $mysqli;
	public function __construct() {
		$this->mysqli = new mysqli(HOST, dbUser, dbUserPass, dbName) or die("Unable to connect");
		$this->mysqli->query("SET NAMES 'utf8'");
	}

	public function addNewUser($login, $password, $role){
	   $result =  $this->mysqli->query("INSERT INTO blogUser(id, username, password, role) VALUES (NULL, '$login', '$password', '$role')");
	   return $result;
	}

	public function getUser($login){
	  $result = $this->mysqli->query("SELECT * FROM `blogUser` WHERE `username` = '$login'");
	  return $result;
  	}

  	public function updateUser($updateUserDB,  $newRole){
  		$result = $this->mysqli->query("UPDATE `blogUser` SET `role` = '$newRole' WHERE `username` = '$updateUserDB'");
  		return $result;
  	}

  	public function changeUserPassword($userPassword, $userLoggedName){
  		$result = $this->mysqli->query("UPDATE `blogUser` SET `password` = '$userPassword' WHERE `username` = '$userLoggedName'");
  		return $result;
  	}
  	public function changeUserLogin($userLogin, $userLoggedName){
  		$result = $this->mysqli->query("UPDATE `blogUser` SET `username` = '$userLogin' WHERE `username` = '$userLoggedName'");
  		return $result;
  	}

  	 public function deleteUser($deleteLogin){
       $result = $this->mysqli->query("DELETE FROM `blogUser` WHERE `username` = '$deleteLogin'");
       return $result;
    }
    public function getAllUsers(){
    	$result = $this->mysqli->query("SELECT * FROM `blogUser`");
	  	return $result;
    }

	public function __destruct() {
		if ($this->mysqli) {
			$this->mysqli->close();
		}
	}
}