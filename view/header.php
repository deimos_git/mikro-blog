<?php 
	session_start();
 ?>
<!DOCTYPE html>
<html lang="en">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title><?= $_SESSION['head-title'] ?><?=$onePost['title']?></title>

     <!--Icons  -->
     <script src="https://use.fontawesome.com/921a81dd54.js"></script>
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/css/bootstrap.min.css" integrity="sha384-/Y6pD6FV/Vv2HJnA6t+vslU6fwYXjCFtcEpHbNJ0lyAFsXTsjBbfaDjzALeQsN6M" crossorigin="anonymous">
    <link rel="stylesheet" href="../dist/css/style.css">
</head>
<body>
<nav class="navbar navbar-expand-md navbar-dark bg-dark">
  <div class="container-fluid">
  <a class="navbar-brand" href="../index.php">Blog</a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>
  <div class="collapse navbar-collapse" id="navbarNav">
   <!--  <ul class="navbar-nav">
     <li class="nav-item active">
        <a class="nav-link" href="index.php">Home <span class="sr-only">(current)</span></a>
      </li> 
      <li class="nav-item">
        <a class="nav-link" href="#">Features</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="#">Pricing</a>
      </li>
    </ul> -->
  </div>
  <div class="navbar-collapse justify-content-end">
  	<ul class="navbar-nav justify-content-between">
  		<li class="nav-item">
  			<?php 
  				if(!empty($_SESSION)){
  					if(isset($_SESSION['logged']) && $_SESSION['role'] == 'admin'){
  						?> 
              <a class="btn btn-outline-primary profile" href="../profile.php"><?php echo "<b>" . $_SESSION['username'] . "</b>" ?></a>
  						<a class="btn btn-outline-primary admin" href="../admin.php">Admin panel</a>
  						<a class="btn btn-outline-primary" href="../profile.php?exit=exit">Logout</a>
  						<?php
  					} elseif(isset($_SESSION['logged'])){
  						?>
  						<a class="btn btn-outline-primary" href="../profile.php"><?php echo "<b>" . $_SESSION['username'] . "</b>" ?></a>
  						<a class="btn btn-outline-primary" href="../profile.php?exit=exit">Logout</a>
  						<?php
  					} else{
	  						?>
	  						<a class="btn btn-outline-primary" href="../profile.php">Sign in</a>
	  						<?php
	  					}
  				}
  			?>
  			
  		</li>
  	</ul>
  </div>
</div>
</nav>