<?php 
session_start();
$_SESSION['head-title'] = 'Blog Post: ';

if(!empty($_GET)){
	$postID = $_GET['post_id'];
	require_once '../controller/postsController.php';
	$post = new postController();
	$onePost = $post->getAllPost($postID);

}

require_once 'header.php';
?>

 <div class="container">
    <div class="row">
        <div class="col-md">
            <div class="page-header">
                <h1><?=$onePost['title']?></h1>
            </div>
            <ul class="list-inline">
                <li><i class="glyphicon glyphicon-calendar"></i> <?=$onePost['author']?> | <?=$onePost['date']?>
            </ul>
            <hr>
            <div class="row">
                <div class="col-md">
                    <?php
                    if (empty($onePost['image'])) {
                        $onePost['image'] = '/dist/uploads/default.jpg';
                    }
                    ?>
                    <div class="post-content">
                        <img src="<?= '/'.$onePost['image']?>" style="padding: 0 10px 10px 0;">
                    </div>
                </div>
            </div>
               <div class="row">
               	<div class="col-md">
               		 <p><?=$onePost['fullPost']?></p>
                </div>
               </div>
        </div>
    </div>
    <div class="row">
        <div class="col-sm">
        <?php
            require_once "comments.php";
        ?>
        </div>
    </div>
 </div>
<?php
require_once 'footer.php';
 ?>