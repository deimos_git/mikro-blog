<?php
session_start();
	$_SESSION['head-title'] = 'Blog profile';
	// echo "<pre>";
	// print_r($_SESSION);
	// echo "</pre>";

if (isset($_GET['exit']) and $_GET['exit'] == 'exit') {
    session_unset();
    header('Location: http://blog.loc/profile.php');
}

	
	if(isset($_POST['auth']) && isset($_POST['auth-password'])){
		$authLogin = $_POST['auth'];
		$authPass = $_POST['auth-password'];

		require_once 'controller/userController.php';
		$authUser = new userController();

		$authResult = $authUser->checkUser($authLogin);

		$loggedUser = $authResult['username'];
		$loggedPassword = $authResult['password'];
		$loggedRole = $authResult['role'];
		

		if($authLogin == $loggedUser and password_verify($authPass, $loggedPassword)){
			$_SESSION['username'] = $loggedUser;
			$_SESSION['role'] =	$loggedRole;
			$_SESSION['logged'] = true;
		} else{
			$authError = "<p class = 'error-message'>Incorrect Login or password</p>";
			$_SESSION['authError'] =  $authError;
		}
	}

	/*
		userRoom Begins
    */
	
	if(isset($_POST['change-user-password'])){ // change password
		if(isset($_POST['user-update-password']) and $_POST['user-update-password'] != ''
			and isset($_POST['user-update-re-password']) and $_POST['user-update-re-password'] != ''){
			$userUpdatePassword = trim(strip_tags($_POST['user-update-password']));
			$userUpdateREPassword = trim(strip_tags($_POST['user-update-re-password']));

				if($userUpdatePassword == $userUpdateREPassword){
					$userLoggedName = $_SESSION['username'];

					$userUpdatePassword = password_hash($userUpdatePassword, PASSWORD_DEFAULT);
					require_once 'controller/userController.php';
					$changeUserPassword = new userController();

					$setUserPass = $changeUserPassword->updateUserPassword($userUpdatePassword, $userLoggedName);
					$userUpdateSuccess = "<p class = 'complate-message'>Password changed</p>";
					$_SESSION['userUpdatePassSuccess'] = $userUpdateSuccess;
				} else{
					$userREPASSFail = "<p class = 'error-message'>Password not match</p>";
					$_SESSION['userREPASSFail'] = $userREPASSFail;
				}
			} else{
				$userUpdateFail = "<p class = 'error-message'>Error</p>";
				$_SESSION['userUpdatePassFail'] = $userUpdateFail;
			}
	}

	if(isset($_POST['change-user-login'])){ // change Login
		if(isset($_POST['user-update-login']) and $_POST['user-update-login'] != ''){
			$userUpdateLogin = trim(strip_tags($_POST['user-update-login']));
				if($_SESSION['username'] != 'admin'){

					$userLoggedName = $_SESSION['username'];

					require_once 'controller/userController.php';
					$changeUserLogged = new userController();

					$setUserLogin = $changeUserLogged->updateUserLogin($userUpdateLogin, $userLoggedName);
					$_SESSION['username'] = $setUserLogin;
					$userUpdateLoginSuccess = "<p class = 'complate-message'>Success</p>";
					$_SESSION['userUpdateLoginSuccess'] = $userUpdateLoginSuccess;
				} else{
					$_SESSION['updateAdmin'] = "<p class = 'error-message'>This user can not be edited</p>";
				}
			} else{
				$userUpdateLoginFail = "<p class = 'error-message'>Error</p>";
				$_SESSION['userUpdateLoginFail'] = $userUpdateLoginFail;
			}
	}
    /*
	userRoom End
    */


	require_once 'view/header.php';
?>


<div class="container">
		<?php 
				if(!isset($_SESSION['logged'])){
					?>
					<div class="col-md-12">
						<h2>Authorization</h2>
						<form action="profile.php" method="POST">
							<div class="form-group">
								<label for="login">Login</label>
								<input type="text" name="auth" autofocus class="form-control" placeholder="Login">
							</div>
							<div class="form-group">
								<label for="password">Password</label>
								<input type="password" name="auth-password" class="form-control" placeholder="Password">
							</div>
							<button type="submit" name="send" class="btn btn-default">Sign in</button>
						</form>
						<?php 
							if(!empty($_SESSION)){
								if(isset($_SESSION['authError'])){
									echo $_SESSION['authError'];
									unset($_SESSION['authError']);
								}
							}
						 ?>
						<hr>
				</div>

					<?php
				} else{
					?>
		<div class="row">
				<div class="col-xs-12">
					<h2>This is the settings page</h2>
				</div>
		</div>
		<div class="row">
			<!-- <div class="col-xs-8"></div> -->
			<div class="col-xs-4 ml-auto">
				<!-- Button trigger modal -->
				<button type="button" class="btn btn-primary" data-toggle="modal" data-target="#ChangePassUser">
				  Change Password
				</button>

				<!-- Modal -->
				<div class="modal fade" id="ChangePassUser" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
				  <div class="modal-dialog" role="document">
					<div class="modal-content">
					  <div class="modal-header">
						<h5 class="modal-title" id="exampleModalLabel">Change Password</h5>
						<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						  <span aria-hidden="true">&times;</span>
						</button>
					  </div>
					  <div class="modal-body">
						<form action="profile.php" method="POST">
							<div class="form-group">
								<input type="password" name="user-update-password" class="form-control" placeholder="password" autofocus>
							</div>
							<div class="form-group">
								<input type="password" name="user-update-re-password" class="form-control" placeholder=" Re - password">
							</div>
					  </div>
					  <div class="modal-footer">
						<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
						<button type="submit"  name = "change-user-password" class="btn btn-primary">Change</button>
					  </div>
						</form>
					</div>
				  </div>
				</div>
			</div>

			<div class="col-xs-4 ml-auto">
				<!-- Button trigger modal -->
				<button type="button" class="btn btn-primary" data-toggle="modal" data-target="#ChangeLoginUser">
				  Change Login
				</button>

				<!-- Modal -->
				<div class="modal fade" id="ChangeLoginUser" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
				  <div class="modal-dialog" role="document">
					<div class="modal-content">
					  <div class="modal-header">
						<h5 class="modal-title" id="exampleModalLabel">Change Login</h5>
						<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						  <span aria-hidden="true">&times;</span>
						</button>
					  </div>
					  <div class="modal-body">
						<form action="profile.php" method="POST">
							<div class="form-group">
								<label for="user-update-login">Change Login</label>
								<input type="text" name="user-update-login" class="form-control" placeholder="Login" autofocus>
							</div>
					  </div>
					  <div class="modal-footer">
						<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
						<button type="submit"  name = "change-user-login" class="btn btn-primary">Change</button>
					  </div>
						</form>
					</div>
				  </div>
				</div>
			</div>
		</div>
	</div>
					<?php
				}
				if(!empty($_SESSION)){
					require_once 'controller/profileInfo.php';
				}
			 ?>

<?php 
	require_once 'view/footer.php';
 ?>