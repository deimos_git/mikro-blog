<?php
session_start();

require_once "model/dbManager.php";

class userController{

	public function __construct(){
		// TODO: Implement __construct() method.
	}

	function getNewUser($newUser, $password, $role){
		$this->$newUser = $newUser;
		$this->$password = $password;
		$this->$role = $role;


		if (mb_strlen($newUser) >= 3 and mb_strlen($newUser) <= 16) {
			$this->$password = password_hash($password, PASSWORD_DEFAULT);
			
			$db = new  dbManager();
			
			

			$newUserResult = $db->addNewUser($this->$newUser, $this->$password, $this->$role);
			if ($newUserResult == 1){
				$userMessage = "<p class = 'complate-message'>User successfully added!</p>";
				$_SESSION['userMessage'] = $userMessage;
			}
			else if($newUserResult == 0){
				$userErrorMessage = "<p class = 'error-message'>User not added!</p>";
				$_SESSION['userErrorMessage'] = $userErrorMessage;
			}

		}
		else{
			$loginError =  "<p class = 'error-message'>Incorrect ligin or password!</p>";
			$_SESSION['loginError'] = $loginError;
		}
	}

	public function checkUser($login){
		$this->$login = $login;
		$db = new  dbManager();
		$checkResult = $db->getUser($this->$login);
		$checkResult = mysqli_fetch_assoc($checkResult);
		return $checkResult;
	}

	public function update($updateUserDB, $newRole){
		$this->$updateUserDB = $updateUserDB;
		$this->$newRole = $newRole;
		$updateCheck = $this->checkUser($updateUserDB);
		$updateCheckUser = $updateCheck['username'];

		if($updateUserDB == $updateCheckUser){
			$db = new dbManager();
			$replace = $db->updateUser($updateUserDB, $newRole);
			$updateUserComplate = "<p class = 'complate-message'>User changed</p>";
			$_SESSION['updateUserComplate'] = $updateUserComplate;
		} else{
			$updateUserFail = "<p class = 'error-message'>Such user does not exist!</p>";
			$_SESSION['updateUserFail'] = $updateUserFail;
		}
	}

	public function updateUserPassword($userPassword, $userLoggedName){ // userRoom Password
		$this->$userPassword = $userPassword;
		$this->$userLoggedName = $userLoggedName;
		

		$db = new dbManager();
		$changeUserSetting = $db->changeUserPassword($userPassword, $userLoggedName);

	}

	public function updateUserLogin($userLogin, $userLoggedName){ // userRoom Login
		$this->$userLogin = $userLogin;
		$this->$userLoggedName = $userLoggedName;
		
		$changeCheckUser = $_SESSION['username'];
		
		if($userLogin != changeCheckUser){
			$db = new dbManager();
			$changeUserSetting = $db->changeUserLogin($userLogin, $userLoggedName);

			$updateCheck = $this->checkUser($userLogin);
			$updateCheckUser = $updateCheck['username'];
			return $updateCheckUser;

			$successChangeUser = "<p class = 'complate-message'>Login changed</p>";
			$_SESSION['successChangeUser'] = $successChangeUser;
		} else{
			$FailChangeUser = "<p class = 'error-messange'>login is busy</p>";
			$_SESSION['FailChangeUser'] = $FailChangeUser;
		}
	}

	public function delete($deleteLogin){
		$this->$deleteLogin = $deleteLogin;
		$delUserCheck = $this->checkUser($deleteLogin);
		$delUserLogin = $delUserCheck['username'];

		if($delUserLogin == $deleteLogin){
			$delMessage = "<p class = 'complate-message'>User deleted!</p>";
			$_SESSION['delMessage'] = $delMessage;
			$db = new dbManager();
			$deleteUser = $db->deleteUser($deleteLogin);

		}else{
				$delErrorMessega = "<p class = 'error-message'>Such user does not exist!</p>";
				$_SESSION['delErrorMessega'] = $delErrorMessega;
			}	
	}

	public function getUsers(){
		$db = new dbManager();
		$result = $db->getAllUsers();
		$result = mysqli_fetch_all($result, MYSQLI_ASSOC);
		return $result;
	}

	public function __destruct(){
		// TODO: Implement __destruct() method.
	}
}