<?php 
session_start();

$ref = $_SERVER['PHP_SELF'];

if ($ref == "/view/post.php"){
    require_once "../model/postsManager.php";
}
else{
    require_once "model/postsManager.php";
}

	class postController{
//        private static $counter = 0;
		public function __construct(){
			// TODO: Implement __construct() method.
//           echo self::$counter +=1;
		}

		public function CreateNewPost($authorPost,$titlePost,$excerptPost,$newPost,$imgFullPath){
			$this->$authorPost = $authorPost;
			$this->$titlePost = $titlePost;
			$this->$excerptPost = $excerptPost;
			$this->$newPost = $newPost;
			$this->$imgFullPath = $imgFullPath;

			$db = new dbPostManager();

			$newPost = $db->CreatePost($authorPost,$titlePost,$excerptPost,$newPost,$imgFullPath);
			if($newPost == 1){
				$AddedPost = "<p class = 'complate-message'>Post added</p>";
				$_SESSION['AddedPost'] = $AddedPost;
			} else{
				$crashPost = "<p class = 'error-message'>Crash Post</p>";
				$_SESSION['crashPost'] = $crashPost;
			}
		}


		public function outputPosts($start, $count){
			$db = new dbPostManager();
			$result = $db->Output($start, $count);
			$result = mysqli_fetch_all($result, MYSQLI_ASSOC);
			return $result;
		}

		public function getAllPost($postID){
			$this->$postID = $postID;

			$db = new dbPostManager();
			$result = $db->getFullPost($postID);
			$result = mysqli_fetch_assoc($result);
			return $result;
		}
		public function delPost ($delPost){
	        $this->$delPost = $delPost;
	        $db = new dbPostManager();
	        $result =  $db->delPost($delPost);
	        if ($result){
	            $_SESSION['deleteComplete'] = "<span class='postDele'>Post deleted!</span>";
	        }
	        else{
	            $_SESSION['deleteInComplete'] = "<span class='postDele'>Post not deleted!</span>";
	        }
	    }

	    public function sortPostA($author){
			$this->$author=$author;
			$db = new dbPostManager();
			$result = $db->sortPostByAuthor($author);
			$result = mysqli_fetch_all($result,MYSQLI_ASSOC);
			return $result;
		}

		public function __destruct(){
			// TODO: Implement __destruct() method.
		}
	}
 ?>